#! /bin/bash

echo I am
whoami

echo The current working directory is
pwd

echo The system I am on is
hostname

echo The Linux version is 
uname -r

echo The Linux distribution is
cat /etc/*-release | grep release | head -1

echo The system has been up for
uptime

echo The amount of disk space I\'m using in KB is
du -k ~ | tail -1
