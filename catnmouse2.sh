#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

catGuess=0

while [ $catGuess -ne $THE_NUMBER_IM_THINKING_OF ]
do
   echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:"
   read catGuess

   if [ $catGuess -lt 1 ]; then 

      echo "You must enter a number that's >= 1"
      continue

   elif [ $catGuess -gt $THE_MAX_VALUE ]; then

      echo "You must enter a number that's <= $THE_MAX_VALUE"
      continue

   elif [ $catGuess -gt $THE_NUMBER_IM_THINKING_OF ]; then

      echo "NO cat... the number I'm thinking of is smaller than $catGuess"
      continue

   elif [ $catGuess -lt $THE_NUMBER_IM_THINKING_OF ]; then
      
      echo "NO cat... the number I'm thinging of is larger than $catGuess"
      continue

   fi

   if [ $catGuess -eq $THE_NUMBER_IM_THINKING_OF ]; then
     
      echo "You got me"
      echo "      \    /\\  "
      echo "       )  ( ')  "
      echo "      (  /  )   "
      echo "       \(__)|   "
      break

   fi

done
